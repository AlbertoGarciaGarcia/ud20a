package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.w3c.dom.events.Event;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class views extends JFrame {
	
	//Inicio todas las variables que necessito
	private static final JToggleButton btn1 = new JToggleButton(""), btn2 = new JToggleButton(""), btn3 = new JToggleButton(""), btn4 = new JToggleButton(""), 
									   btn5 = new JToggleButton(""), btn6 = new JToggleButton(""), btn7 = new JToggleButton(""), btn8 = new JToggleButton(""),
									   btn9 = new JToggleButton(""), btn10 = new JToggleButton(""), btn11 = new JToggleButton(""), btn12 = new JToggleButton("");
	
	private static boolean resueltoBtn1, resueltoBtn2, resueltoBtn3, resueltoBtn4, resueltoBtn5, resueltoBtn6, resueltoBtn7, resueltoBtn8, resueltoBtn9, resueltoBtn10, resueltoBtn11, resueltoBtn12;
	
	private static int casilla1 = 0, casilla2 = 0, contadorCasillas = 0;
	
	public views() {
		getContentPane().setLayout(null);
		setBounds(400,200,452,418);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(true);
		
		//Boton1
		btn1.setSelected(true);
		btn1.setBackground(Color.RED);
		btn1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(1);
			}
		});
		btn1.setBounds(39, 70, 80, 80);
		getContentPane().add(btn1);
		
		//Boton2
		btn2.setSelected(true);
		btn2.setBackground(Color.BLUE);
		btn2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(2);
			}
		});
		btn2.setBounds(130, 70, 80, 80);
		getContentPane().add(btn2);
		
		//Boton3
		btn3.setSelected(true);
		btn3.setBackground(Color.GREEN);
		btn3.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(3);
			}
		});
		btn3.setBounds(220, 70, 80, 80);
		getContentPane().add(btn3);
		
		//Boton4
		btn4.setSelected(true);
		btn4.setBackground(Color.GREEN);
		btn4.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(4);
			}
		});
		btn4.setBounds(310, 70, 80, 80);
		getContentPane().add(btn4);
		
		//Boton5
		btn5.setSelected(true);
		btn5.setBackground(Color.RED);
		btn5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(5);
			}
		});
		btn5.setBounds(39, 158, 80, 80);
		getContentPane().add(btn5);
		
		//Boton6
		btn6.setSelected(true);
		btn6.setBackground(Color.YELLOW);
		btn6.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(6);
			}
		});
		btn6.setBounds(130, 158, 80, 80);
		getContentPane().add(btn6);
		
		//Boton7
		btn7.setSelected(true);
		btn7.setBackground(Color.BLACK);
		btn7.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(7);
			}
		});
		btn7.setBounds(220, 158, 80, 80);
		getContentPane().add(btn7);
		
		//Boton8
		btn8.setSelected(true);
		btn8.setBackground(Color.ORANGE);
		btn8.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(8);
			}
		});
		btn8.setBounds(310, 158, 80, 80);
		getContentPane().add(btn8);
		
		//Boton9
		btn9.setSelected(true);
		btn9.setBackground(Color.ORANGE);
		btn9.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(9);
			}
		});
		btn9.setBounds(39, 249, 80, 80);
		getContentPane().add(btn9);
		
		//Boton10
		btn10.setSelected(true);
		btn10.setBackground(Color.BLACK);
		btn10.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(10);
			}
		});
		btn10.setBounds(130, 249, 80, 80);
		getContentPane().add(btn10);
		
		//Boton11
		btn11.setSelected(true);
		btn11.setBackground(Color.YELLOW);
		btn11.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(11);
			}
		});
		btn11.setBounds(220, 249, 80, 80);
		getContentPane().add(btn11);
		
		//Boton12
		btn12.setSelected(true);
		btn12.setBackground(Color.BLUE);
		btn12.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				operacionesCasilla(12);
			}
		});
		btn12.setBounds(310, 249, 80, 80);
		getContentPane().add(btn12);

	}
	
	public static void operacionesCasilla(int casilla) {
		
		if (contadorCasillas == 0) {
			casilla1 = casilla;
			contadorCasillas++;
		}else if (contadorCasillas == 1) {
			casilla2 = casilla;
			contadorCasillas++;
		}else if (contadorCasillas == 2) {
			comprovarCasilla(casilla1, casilla2);
			resetearValores();
		}
		
	}
	
	public static void comprovarCasilla(int casilla1, int casilla2) {
		if (casilla1 == 1 && casilla2 == 5) {
			resueltoBtn1 = true;
			resueltoBtn5 = true;
			btn1.setEnabled(false);
			btn5.setEnabled(false);
		}else if (casilla1 == 2 && casilla2 == 12) {
			resueltoBtn2 = true;
			resueltoBtn12 = true;
			btn2.setEnabled(false);
			btn12.setEnabled(false);
		}else if (casilla1 == 3 && casilla2 == 4) {
			resueltoBtn3 = true;
			resueltoBtn4 = true;
			btn3.setEnabled(false);
			btn4.setEnabled(false);
		}else if (casilla1 == 6 && casilla2 == 11) {
			resueltoBtn6 = true;
			resueltoBtn11 = true;
			btn6.setEnabled(false);
			btn11.setEnabled(false);
		}else if (casilla1 == 7 && casilla2 == 10) {
			resueltoBtn7 = true;
			resueltoBtn10 = true;
			btn7.setEnabled(false);
			btn10.setEnabled(false);
		}else if (casilla1 == 8 && casilla2 == 9) {
			resueltoBtn8 = true;
			resueltoBtn9 = true;
			btn8.setEnabled(false);
			btn9.setEnabled(false);
		}
	}
	
	public static void resetearValores() {
		casilla1 = 0;
		casilla2 = 0;
		contadorCasillas = 0;
		if (!resueltoBtn1) {
			btn1.setSelected(true);
		}
		if(!resueltoBtn2) {
			btn2.setSelected(true);
		}
		if(!resueltoBtn3) {
			btn3.setSelected(true);
		}
		if(!resueltoBtn4) {
			btn4.setSelected(true);
		}
		if(!resueltoBtn5) {
			btn5.setSelected(true);
		}
		if(!resueltoBtn6) {
			btn6.setSelected(true);
		}
		if(!resueltoBtn7) {
			btn7.setSelected(true);
		}
		if(!resueltoBtn8) {
			btn8.setSelected(true);
		}
		if(!resueltoBtn9) {
			btn9.setSelected(true);
		}
		if(!resueltoBtn10) {
			btn10.setSelected(true);
		}
		if(!resueltoBtn11) {
			btn11.setSelected(true);
		}
		if(!resueltoBtn12) {
			btn12.setSelected(true);
		}
	}
}

package Vista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.Font;

public class Vista extends JFrame {
	
	public Vista() {
		getContentPane().setLayout(null);
		setBounds(400,200,351,393);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		//El texto para insertar el primer valor
		textoValor1 = new JTextField();
		textoValor1.setBounds(40, 186, 86, 20);
		getContentPane().add(textoValor1);
		textoValor1.setColumns(10);
	
		//El texto para insertar el segundo valor
		textoValor2 = new JTextField();
		textoValor2.setColumns(10);
		textoValor2.setBounds(40, 258, 86, 20);
		getContentPane().add(textoValor2);
		
		
		//Los dos textos que salen
		JLabel valor1 = new JLabel("Valor 1");
		valor1.setBounds(40, 161, 46, 14);
		getContentPane().add(valor1);
		
		JLabel valor2 = new JLabel("Valor 2");
		valor2.setBounds(40, 233, 46, 14);
		getContentPane().add(valor2);
		
		//El boton suma con la operacion
		JButton btnSuma = new JButton("+");
		btnSuma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSuma.setBounds(249, 169, 51, 51);
		getContentPane().add(btnSuma);
		btnSuma.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				System.out.println(Integer.parseInt(textoValor1.getText()) + Integer.parseInt(textoValor2.getText()));
			}	
		});

		//El boton resta con la operacion
		JButton btnResta = new JButton("-");
		btnResta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnResta.setBounds(188, 169, 51, 51);
		getContentPane().add(btnResta);
		btnResta.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				System.out.println(Integer.parseInt(textoValor1.getText()) - Integer.parseInt(textoValor2.getText()));
			}	
		});
		
		//El boton de multiplicacin con la operacin
		JButton btnMultip = new JButton("*");
		btnMultip.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnMultip.setBounds(249, 227, 51, 51);
		getContentPane().add(btnMultip);
		btnMultip.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				System.out.println(Integer.parseInt(textoValor1.getText()) * Integer.parseInt(textoValor2.getText()));
			}	
		});

		//El boton de la divisin con la operacin
		JButton btnDivision = new JButton("/");
		btnDivision.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDivision.setBounds(188, 227, 51, 51);
		getContentPane().add(btnDivision);
		btnDivision.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				System.out.println(Integer.parseInt(textoValor1.getText()) / Integer.parseInt(textoValor2.getText()));
			}	
		});
		
	}
	
	private JTextField textoValor1;
	private JTextField textoValor2;
	
}

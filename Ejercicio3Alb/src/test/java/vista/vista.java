package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class vista extends JFrame {

	private JPanel contentPane;
	private JButton boton1;
	private JButton boton2;
	
	public vista() {
		setTitle("Contador Botones 1 y 2 ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();	// Creamos el panel
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		boton1 = new JButton("Boton 1"); // Creamos el boton 1
		boton1.setBounds(49, 132, 122, 52);
		contentPane.add(boton1);
		boton2 = new JButton("Boton 2"); // Creamos el boton 2
		boton2.setBounds(249, 132, 122, 52);
		contentPane.add(boton2);
		
		JLabel tituloboton1 = new JLabel("Boton 1 : 0 veces"); // Creamos la primera label
		tituloboton1.setBounds(56, 88, 115, 14);
		contentPane.add(tituloboton1);
		
		JLabel tituloboton2 = new JLabel("Boton 2 : 0 veces"); // Creamos la segunda label
		tituloboton2.setBounds(263, 88, 109, 14);
		contentPane.add(tituloboton2);
		setVisible(true);
		
		
		realizarAccion boton1accion = new realizarAccion(tituloboton1); // Hacemos un objeto pasando el titulo que cambia depende del boton
		realizarAccion boton2accion = new realizarAccion(tituloboton2); // Lo mismo con el segundo
		boton1.addActionListener(boton1accion); // Añadimos el action listener para que el boton haga algo al ser pulsado
		boton2.addActionListener(boton2accion);
		
		
	
	}
	
	public  class realizarAccion implements ActionListener {
		private int numVeces;
		private JLabel titulo;

		public realizarAccion(JLabel titulo) {
		numVeces = 0;
		this.titulo = titulo;

		}
		
		
		public void actionPerformed(ActionEvent botonpulsado) {
		numVeces++;
		JButton boton = (JButton) botonpulsado.getSource(); // Cada vez que sea pulsado
		titulo.setText(boton.getText() + " : " + numVeces +  " veces"); // Cambia el titulo dependiendo del que se le haya pasado
		}

		}
	
	
}

package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	// Creo la constante
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;

	public Principal() {
		setTitle("Convertidor de moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// Hacemos que el panel sea visible
		setVisible(true);
		
		// Creo un label
		JLabel lblNewLabel = new JLabel("Cantidad a convertir:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(29, 54, 138, 16);
		contentPane.add(lblNewLabel);
		
		// Le doy la forma al textField
		textField = new JTextField();
		textField.setBounds(179, 51, 91, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		// Creo un label 
		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblResultado.setBounds(282, 54, 76, 16);
		contentPane.add(lblResultado);
		
		// Modifico el textField_1
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(370, 51, 91, 22);
		contentPane.add(textField_1);
		
		// Modifico el btnNewButton
		btnNewButton = new JButton("Euros a pesetas");
		// Creo un ActionListener
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Seteo el texto del textfield_1 llamando al método conversion y transformandolo en
				// String
				textField_1.setText(Double.toString(conversion()));
			}
		});
		btnNewButton.setBounds(119, 117, 134, 33);
		contentPane.add(btnNewButton);
		
		// Creo el botón cambiar
		JButton btnCambiar = new JButton("Cambiar");
		// Creo el ActionListener
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Para cambiar el texto e informar al usuario llamando al método cambiarTexto
				cambiarTexto();
			}
		});
		btnCambiar.setBounds(282, 117, 134, 33);
		contentPane.add(btnCambiar);
	}
	
	// Creo el método cambiarTexto
	public void cambiarTexto() {
		
		// Si el texto del botón equivale a esto
		if (btnNewButton.getText().equalsIgnoreCase("Euros a pesetas")) {
			// Lo seteo a esto
			btnNewButton.setText("Pesetas a euros");
		}
		else {
			// Si el texto no equivale al primero lo seteo a esto
			btnNewButton.setText("Euros a pesetas");
		}
		
	}
	
	// Creo el método conversion
	public double conversion() {
		
		// Inicializo el resultado a 0
		double resultado = 0;
		
		// Creo un if que si el texto equivale a esto
		if (btnNewButton.getText().equalsIgnoreCase("Euros a pesetas")) {
			// Calcule los euros a pesetas pasando el texto a double
			resultado = Double.parseDouble(textField.getText()) * 166.386;
		}
		else {
			// Si no, que calcule de pesetas a euros pasando el texto a double
			resultado = Double.parseDouble(textField.getText()) * 0.006;
		}
		
		// Retorno el resultado
		return resultado;
	}
}

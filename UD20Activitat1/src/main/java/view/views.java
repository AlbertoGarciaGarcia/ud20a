package view;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class views extends JFrame {

	
	private JLabel etiqueta;
	
	public views() {
		getContentPane().setLayout(null);
		
		etiqueta = new JLabel("titulo");
		etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
		etiqueta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		etiqueta.setBounds(95, 78, 75, 31);
		getContentPane().add(etiqueta);
		
		JButton btnTexto = new JButton("Presioname");
		btnTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				etiqueta.setFont(new Font("Tahoma", Font.PLAIN, 28));
			}
		});
		btnTexto.setBounds(74, 120, 119, 39);
		getContentPane().add(btnTexto);
		
		setBounds(400,200,305,352);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(true);
		
	}
}

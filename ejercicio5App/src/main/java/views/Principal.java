package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	// Creo la constante
	private JTextField textField;

	
	public Principal() {
		setTitle("Eventos de ratón");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 764, 405);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// Hago que el panel sea visible
		setVisible(true);
		
		// Creo el botón que actuará de limpiador
		JButton btnNewButton = new JButton("Limpiar");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		// Creo un ActionListener
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Que llame a la función limpiarTexto
				limpiarTexto();
			}
		});
		btnNewButton.setBounds(301, 13, 127, 54);
		contentPane.add(btnNewButton);
		
		// Le doy valores a la constante que he creado antes
		textField = new JTextField();
		// Creo un MouseListener para que cuando el ratón haga alguna acción
		// Escriba algo
		textField.addMouseListener(new MouseAdapter() {
			
			// Cuando se clique al ratón
			public void mouseClicked(MouseEvent e) {
				
				// Seteo el texto del campo llamando a la función getTexto y 
				// sumándole el texto que yo quiera
				textField.setText(getTexto() + "Has clicado dentro del componente");
			}
			
			// Cuando el ratón entre al componente
			public void mouseEntered(MouseEvent e) {
				// Seteo el texto del campo llamando a la función getTexto y 
				// sumándole el texto que yo quiera
				textField.setText(getTexto() + "El ratón ha entrado en el componente");
			}
			
			// Cuando el ratón salga del componente
			public void mouseExited(MouseEvent e) {
				// Seteo el texto del campo llamando a la función getTexto y 
				// sumándole el texto que yo quiera
				textField.setText(getTexto() + "El ratón ha salido del componente");
			}
			
			// Cuando el ratón se presione
			public void mousePressed(MouseEvent e) {
				// Seteo el texto del campo llamando a la función getTexto y 
				// sumándole el texto que yo quiera
				textField.setText(getTexto() + "El ratón ha sido presionado");
			}
			
			// Cuando el ratón se suelte
			public void mouseReleased(MouseEvent e) {
				// Seteo el texto del campo llamando a la función getTexto y 
				// sumándole el texto que yo quiera
				textField.setText(getTexto() + "El ratón ha sido soltado");
			}
		});
		textField.setBounds(37, 92, 663, 227);
		contentPane.add(textField);
		textField.setColumns(10);
	}
	
	// Creo la función que limpiará el texto del campo de texto
	public void limpiarTexto() {
		textField.setText(null);
	}
	
	// Creo la función que cogerá el texto que haya en el campo
	// para que no se borre cada vez que se haga una acción con el ratón
	public String getTexto() {
		String texto = "";
		texto = textField.getText();
		return texto;
	}
}

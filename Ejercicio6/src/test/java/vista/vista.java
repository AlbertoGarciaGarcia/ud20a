package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class vista extends JFrame {

	private JPanel contentPane;

	private JTextField textoValor1;
	private JTextField textoValor2;
	private JTextField textField;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public vista() {
		getContentPane().setLayout(null);
		setBounds(400,200,351,215); // Creamos el panel
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		

		// El texto para insertar el altura
		textoValor1 = new JTextField();
		textoValor1.setBounds(38, 36, 86, 20);
		getContentPane().add(textoValor1);
		textoValor1.setColumns(10);
	
		//El texto para insertar el peso
		textoValor2 = new JTextField();
		textoValor2.setColumns(10);
		textoValor2.setBounds(179, 36, 86, 20);
		getContentPane().add(textoValor2);

		
		//El peso y la altura
				JLabel valor1 = new JLabel("Altura ( metros )");
				valor1.setBounds(38, 11, 86, 14);
				getContentPane().add(valor1);
				
				JLabel valor2 = new JLabel("Peso (kg)");
				valor2.setBounds(201, 11, 46, 14);
				getContentPane().add(valor2);
				
		

				// Hacemos el boton de calcular
				JButton btnDivision = new JButton("Calcular IMC");
				btnDivision.setFont(new Font("Tahoma", Font.PLAIN, 16));
				btnDivision.setBounds(20, 67, 153, 51);
				getContentPane().add(btnDivision);
				
				textField = new JTextField();
				textField.setBounds(213, 84, 86, 20);
				getContentPane().add(textField);
				textField.setColumns(10);
				// Hacemos la label
				JLabel lblNewLabel = new JLabel("IMC");
				lblNewLabel.setBounds(183, 87, 46, 14);
				getContentPane().add(lblNewLabel);
				btnDivision.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						double alturaCuadrado = Math.pow(Double.parseDouble(textoValor1.getText()), 2); // Realizamos el calculo
						Double resultado = (Double.parseDouble(textoValor2.getText()) / alturaCuadrado);
						String resultadoTexto = resultado.toString();
						textField.setText(resultadoTexto);
					}	
				});

		
				setVisible(true);
		
		
		
	}

}

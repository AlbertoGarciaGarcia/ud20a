package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	// Creo la constante
	private JLabel lblNewLabel_1;

	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 596, 210);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// Hago que el panel pueda ser visible
		setVisible(true);
		
		// Creo una label
		JLabel lblNewLabel = new JLabel("Has pulsado:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(34, 23, 108, 16);
		contentPane.add(lblNewLabel);
		
		// Creo un botón nuevo que ponga botón 1
		JButton btnNewButton = new JButton("Botón 1");
		// Creo un ActionListener
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Que llame a la función cambiarTextoBotón1
				cambiarTextoBoton1();
			}
		});
		btnNewButton.setBounds(300, 20, 97, 25);
		contentPane.add(btnNewButton);
		
		// Creo un botón nuevo que ponga botón 2
		JButton btnNewButton_1 = new JButton("Botón 2");
		// Creo un ActionListener
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Que llame a la función cambiarTextoBotón2
				cambiarTextoBoton2();
			}
		});
		btnNewButton_1.setBounds(409, 20, 97, 25);
		contentPane.add(btnNewButton_1);
		
		// Le doy características a la constante que he creado antes
		lblNewLabel_1 = new JLabel("Botón 1");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(177, 24, 77, 16);
		contentPane.add(lblNewLabel_1);
	}
	
	// Cuando llamen a esta función cambiará el texto del label1
	public void cambiarTextoBoton1() {
		
		lblNewLabel_1.setText("Botón 1");
		
	}
	
	// Cuando llamen a esta función cambiará el texto del label1
	public void cambiarTextoBoton2() {
		
		lblNewLabel_1.setText("Botón 2");
		
	}
}

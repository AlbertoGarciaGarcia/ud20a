package view;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.prism.paint.Color;

import exceptions.excepciones;

import javax.swing.JButton;

public class views extends JFrame {
	
	// Creo la constante
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	
	public views() {
		setTitle("Convertidor de moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// Hacemos que el panel sea visible
		setVisible(true);
		
		// Creo un label
		JLabel lblNewLabel = new JLabel("Cantidad a convertir:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(29, 54, 138, 16);
		contentPane.add(lblNewLabel);
		
		// Le doy la forma al textField
		textField = new JTextField();
		textField.setBounds(179, 51, 91, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		// Creo un label 
		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblResultado.setBounds(282, 54, 76, 16);
		contentPane.add(lblResultado);
		
		// Modifico el textField_1
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(370, 51, 91, 22);
		contentPane.add(textField_1);
		
		// Modifico el btnNewButton
		btnNewButton = new JButton("Euros a pesetas");
		// Creo un ActionListener
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Seteo el texto del textfield_1 llamando al método conversion y transformandolo en
				// String
				textField_1.setText(Double.toString(conversion()));
			}
		});
		btnNewButton.setBounds(29, 81, 134, 33);
		contentPane.add(btnNewButton);
		
		// Creo el botón cambiar
		JButton btnCambiar = new JButton("Cambiar");
		// Creo el ActionListener
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Para cambiar el texto e informar al usuario llamando al método cambiarTexto
				cambiarTexto();
			}
		});
		
		//Boton borrar
		btnCambiar.setBounds(202, 81, 134, 33);
		contentPane.add(btnCambiar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_1.setText(" ");
				textField.setText(" ");
			}
		});
		btnBorrar.setBounds(370, 81, 134, 33);
		contentPane.add(btnBorrar);
	}
	public void cambiarTexto() {
		
		// Si el texto del botón equivale a esto
		if (btnNewButton.getText().equalsIgnoreCase("Euros a pesetas")) {
			// Lo seteo a esto
			btnNewButton.setText("Pesetas a euros");
		}
		else {
			// Si el texto no equivale al primero lo seteo a esto
			btnNewButton.setText("Euros a pesetas");
		}
		
	}
	public double conversion() {
		
		// Inicializo el resultado a 0
		double resultado = 0;
		
		// Creo un if que si el texto equivale a esto
		if (btnNewButton.getText().equalsIgnoreCase("Euros a pesetas")) {
			// Calcule los euros a pesetas pasando el texto a double
			resultado = (excepciones.confirmarNumero(textField.getText())) * 166.386;
		}
		else {
			// Si no, que calcule de pesetas a euros pasando el texto a double
			resultado = (excepciones.confirmarNumero(textField.getText())) * 0.006;
		}
		
		// Retorno el resultado
		return resultado;
	}
}

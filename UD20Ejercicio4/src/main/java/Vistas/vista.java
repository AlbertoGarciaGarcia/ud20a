package Vistas;

import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.EventQueue;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;


import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class vista extends JFrame implements WindowListener {

	private JPanel panel;
	private JTextArea campoTexto;
	private final String espacio = "\n";

	/**
	 * Launch the application.

	/**
	 * Create the frame.
	 */
	public vista() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 264, 334); // Creamos el panel
		panel = new JPanel();
		addWindowListener(this); // Hacemos que sea el panel el que escucha las acciones, de la funcion windowListener
		pack(); 
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel);
		panel.setLayout(null);
		
		JLabel eventos = new JLabel("Eventos");
		eventos.setBounds(10, 113, 46, 14); // Hacemos el label
		panel.add(eventos);
		
		
		
	
		
		
		
		
		
		campoTexto = new JTextArea();
		campoTexto.setBounds(80, 11, 307, 239);
		panel.add(campoTexto); // Creamos el campo de texto
		campoTexto.setColumns(40);
		setTitle("Reaccion Ventanas");
		setVisible(true);
	}
	public void windowActivated(WindowEvent e) { // Cada vez que se haga una accion, el windowListener activara el WindowEvent e imprimira algo dependiendo de la accion
		campoTexto.append(espacio + "Ventana encendida");

		}
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana abierta");
		
	}
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana cerrandose");
		
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana cerrada");
		
	}
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana iconizada");
		
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana desiconizada");
		
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		campoTexto.append(espacio + "Ventana desactivada");
		
	}
}
